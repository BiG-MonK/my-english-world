const PORT = process.env.PORT || 3000;
let express     = require("express"),
    app         = express(),
    bodyParser  = require("body-parser"),
    mongoose    = require("mongoose"),
    Word        = require("./models/word");

mongoose.connect("mongodb://localhost/My_English_World", {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  });
app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(express.static('views'));

app.get("/", function (req, res) {
  res.render("landing");
});

// INDEX - show all words
app.get("/words", function (req, res) {
  Word.find({}, function(err, words){
    if (err) console.log(err)
    else res.render("index", { words: words });
  })
});

// CREATE - add new word to DB
app.post("/words", function (req, res) {
  let name = req.body.name;
  let translation = req.body.translation;
  let image = req.body.image || "images/placeholder.png";
  let part_of_speech = req.body.part_of_speech;
  let description = req.body.description;
  let example = req.body.example;
  let transcript = req.body.transcript;
  let level = req.body.level;
  let newWord = {
    name: name,
    translation: translation,
    image: image,
    part_of_speech: part_of_speech,
    description: description,
    example: example,
    transcript: transcript,
    level: level
  };
  Word.create(newWord, function(err, newlyCreated){
    if (err) console.log(err)
    else res.redirect("/words");
  })
});

// NEW - show form to create new word
app.get("/words/new", function (req, res) {
  res.render("new.ejs");
});

// SHOW - shows more info about one word
app.get("/words/:id", function(req, res){
  Word.findById(req.params.id, function(err, foundWord){
    if (err) console.log(err)
    else {
      res.render("show", {word: foundWord, path: "../"});
    }
  })
})

app.listen(PORT, process.env.IP, function () {
  console.log(`The English World has started on port ${PORT}!`)
});